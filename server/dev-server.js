import fs from 'fs'
import path from 'path'
import { fileURLToPath } from 'url'
import express from 'express'
import { createServer as createViteServer } from 'vite'
import { getAliasedLocales, getAllLocales } from '../src/shared/i18n.js'

const isProd = process.env.NODE_ENV === 'production'

async function createServer() {
  const app = express()

  const __dirname = path.dirname(fileURLToPath(import.meta.url))
  const resolve = (p) => path.resolve(__dirname, '..', p)

  const indexProd = isProd
    ? fs.readFileSync(resolve('dist/client/index.html'), 'utf-8')
    : ''

  const manifest = isProd
    ? JSON.parse(
        fs.readFileSync(resolve('dist/client/ssr-manifest.json'), 'utf-8')
      )
    : {}

  // Create Vite server in middleware mode and configure the app type as
  // 'custom', disabling Vite's own HTML serving logic so parent server
  // can take control

  let vite

  if (isProd) {
    app.use(
      '/test/',
      (await import('serve-static')).default(resolve('dist/client'), {
        index: false
      })
    )
  } else {
    vite = await createViteServer({
      server: { middlewareMode: true },
      appType: 'custom'
    })

    // use vite's connect instance as middleware
    // if you use your own express router (express.Router()), you should use router.use
    app.use(vite.middlewares)
  }

  app.use('*', async (req, res) => {
    const url = req.originalUrl

    try {
      let template
      let render

      if (!isProd) {
        // always read fresh template in dev
        template = fs.readFileSync(resolve('index.html'), 'utf-8')
        template = await vite.transformIndexHtml(url, template)
        render = (await vite.ssrLoadModule('/src/entry-server.js')).render
      } else {
        template = indexProd
        render = (await import('./dist/server/entry-server.js')).render
      }

      const alias = getAliasedLocales()
      let languageWanted = req.acceptsLanguages(getAllLocales())

      if (languageWanted && alias[languageWanted]) {
        languageWanted = alias[languageWanted]
      }

      const { html: appHtml, preloadLinks, headTags } = await render(url, manifest, languageWanted)

      const html = template
        .replace(`<!--preload-links-->`, preloadLinks)
        .replace(`<!--app-html-->`, appHtml)
        .replace(`<html>`, `<html${headTags.htmlAttrs}>`)
        .replace(`<!--head tags-->`, headTags.headTags)

      res.status(200).set({ 'Content-Type': 'text/html' }).end(html)
    } catch (e) {
      vite && vite.ssrFixStacktrace(e)
      console.error(e)
      res.status(500).end(e.stack)
    }
  })

  app.listen(8042, () => {
    console.log('Ready on 127.0.0.1:8042')
  })
}

createServer()
