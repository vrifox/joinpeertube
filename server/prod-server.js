import express from 'express'
import { pathExists } from 'fs-extra'
import { readdir } from 'fs/promises'
import { join, resolve } from 'path'
import cookieParser from 'cookie-parser'
import { parse as parseURL } from 'url'

if (!process.argv[2]) {
  console.error('Missing root directory.')
  process.exit(-1)
}

const baseStaticDir = resolve(process.argv[2])

const resolveStaticFile = p => join(baseStaticDir, p)

const defaultLocale = 'en_US'

console.log('Serving files from %s', baseStaticDir)
console.log('Using %s as default locale', defaultLocale)

async function createServer() {
  const app = express()
  app.use(cookieParser())

  const allLocales = await buildAvailableLocales(baseStaticDir)
  const allLocalePaths = allLocales.map(l => `/${l}`)

  console.log('Detected following locales: ' + allLocales.join(', '))

  for (const path of [ 'assets', 'img' ]) {
    app.use(`/${path}`, express.static(resolveStaticFile(path)))
  }

  app.get('/apple-touch-icon.png', express.static(resolveStaticFile('img/icons')))

  app.use('/api/', express.static(resolveStaticFile('api')))
  app.get('/*.xml', express.static(baseStaticDir))
  app.get('/*.zip', express.static(baseStaticDir))

  app.use(allLocalePaths, async (req, res) => {
    const locale = extractLocaleFromUrl(req.originalUrl)

    res.cookie('locale', locale, {
      secure: process.env.NODE_ENV === 'production',
      sameSite: 'none',
      maxAge: 1000 * 3600 * 24 * 90 // 3 months
    })

    return serveHTML({
      url: req.originalUrl,
      res,
      hasLocaleInUrl: true,
      localeToInject: undefined
    })
  })

  app.use('*', async (req, res) => {
    let locale = defaultLocale

    if (req.cookies.locale && allLocales.includes(req.cookies.locale)) {
      locale = req.cookies.locale
    } else if (req.headers['accept-language']) {
      locale = req.acceptsLanguages(allLocales) || defaultLocale
    }

    return serveHTML({
      url: req.originalUrl,
      res,
      localeToInject: locale,
      hasLocaleInUrl: false
    })
  })

  const host = '127.0.0.1'
  const port = 8042

  app.listen(port, host, () => {
    console.log('Listening on %s:%d', host, port)
  })
}

createServer()
  .catch(err => {
    console.error(err)
    process.exit(1)
  })

// ---------------------------------------------------------------------------

async function buildAvailableLocales () {
  const entries = await readdir(baseStaticDir, { withFileTypes: true })

  const directories = entries
    .filter(entry => entry.isDirectory())
    .map(entry => entry.name)

  const locales = []

  for (const directory of directories) {
    const content = await readdir(join(baseStaticDir, directory))

    if (!content.includes('index.html')) continue

    locales.push(directory)
  }

  return locales
}

// ---------------------------------------------------------------------------

async function serveHTML (options) {
  const { url, res, hasLocaleInUrl, localeToInject } = options

  try {
    const basePath = hasLocaleInUrl
      ? getUrlPathOnly(url)
      : join(localeToInject, getUrlPathOnly(url))

    const urlHTML = resolveStaticFile(basePath + '.html')
    if (await pathExists(urlHTML)) {
      return res.sendFile(urlHTML)
    }

    const indexHTML = resolveStaticFile(join(basePath, 'index.html'))
    if (await pathExists(indexHTML)) {
      return res.sendFile(indexHTML)
    }

    const basePath404 = hasLocaleInUrl
      ? extractLocaleFromUrl(url)
      : localeToInject

    const fallbackFilePath = resolveStaticFile(join(basePath404, '404.html'))

    console.log('Fallback to %s for URL %s.', fallbackFilePath, url)

    return res.status(404).sendFile(fallbackFilePath)
  } catch (err) {
    console.error('Cannot serve file %s', url, err)

    return res.sendStatus(500)
  }
}

// ---------------------------------------------------------------------------

function extractLocaleFromUrl (url) {
  const splitted = url.split('/')

  if (splitted.length < 2) return undefined

  return splitted[1]
}

function getUrlPathOnly (url) {
  return removeTrailingSlash(parseURL(url).pathname)
}

function removeTrailingSlash(str) {
  return str.endsWith('/')
    ? str.slice(0, -1)
    : str
}
