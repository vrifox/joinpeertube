import { resolve, dirname, join } from 'path'
import { fileURLToPath } from 'url'
import { getAllLocales } from '../src/shared/i18n.js'
import { outputFile, readJson, remove } from 'fs-extra/esm'
import { readFile } from 'fs/promises'
import { render } from '../dist/server/entry-server.js'
import { getLocaleFromPath, isExcludedPath } from '../src/shared/url.js'

run()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err)
    process.exit(-1)
  })

async function run () {
  const { allRoutes } = await render('/')

  const manifest = await readJson(toAbsolute('dist/static/ssr-manifest.json'), 'utf-8')
  const template = await readFile(toAbsolute('dist/static/index.html'), 'utf-8')

  const promises = []

  for (const route of allRoutes) {
    if (isExcludedPath(route, false)) continue
    if (!getLocaleFromPath(route)) continue

    promises.push(prerenderRoute({
      render,
      manifest,
      template,
      route
    }))
  }

  await Promise.all(promises)

  await remove(toAbsolute('dist/static/ssr-manifest.json'))
}

function toAbsolute (p) {
  const __dirname = dirname(fileURLToPath(import.meta.url))

  return resolve(__dirname, '..', p)
}

function getFilePath (url) {
  if (url === '/') return 'index.html'

  for (const locale of getAllLocales()) {
    if (url !== `/${locale}`) continue

    return join(locale, 'index.html')
  }

  return url + '.html'
}

async function prerenderRoute (options) {
  const { render, route, manifest, template } = options

  const locale = getLocaleFromPath(route)
  const { html: appHtml, preloadLinks, headTags } = await render(route, manifest, locale)

  const html = template
    .replace(/<title>.+<\/title>/, '')
    .replace(/<meta property="og:title"[^>]+>/, '')
    .replace(/<meta property="og:description"[^>]+>/, '')
    .replace(/<meta property="og:image"[^>]+>/, '')
    .replace(/<meta name="twitter:title"[^>]+>/, '')
    .replace(/<meta name="twitter:description"[^>]+>/, '')
    .replace(/<meta name="twitter:image"[^>]+>/, '')
    .replace(/<meta name="description"[^>]+>/, '')
    .replace(`<html>`, `<html${headTags.htmlAttrs}>`)
    .replace(`<!--head-tags-->`, headTags.headTags)
    .replace(`<!--preload-links-->`, preloadLinks)
    .replace(`<!--app-html-->`, appHtml)

  const filePath = join('dist', 'static', getFilePath(route))
  await outputFile(toAbsolute(filePath), html)

  console.log('Pre-rendered: ', filePath)
}
