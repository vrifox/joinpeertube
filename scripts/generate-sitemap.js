import { isDefaultLocale } from '../src/shared/i18n.js'
import { isExcludedPath, getLocaleFromPath } from '../src/shared/url.js'
import { render } from '../dist/server/entry-server.js'

const indentation = '    '

run()

async function run () {
  const { allRoutes } = await render('/')

  displaySitemap(allRoutes)
}

// ---------------------------------------------------------------------------

function displaySitemap (paths) {
  let message = ''

  message += '<?xml version="1.0" encoding="UTF-8"?>\n'
  message += '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"\n'
  message += indentation + indentation + 'xmlns:xhtml="http://www.w3.org/1999/xhtml">\n\n'

  const pathsObject = buildPaths(paths)

  for (const rootPath of Object.keys(pathsObject)) {
    message += buildLinkWithAlternates(pathsObject, rootPath, rootPath)

    for (const locale of Object.keys(pathsObject[rootPath])) {
      message += buildLinkWithAlternates(pathsObject, rootPath, pathsObject[rootPath][locale])
    }
  }

  message += '</urlset>\n'

  console.log(message)
}

function buildPaths (paths) {
  const result = {}

  for (const path of paths) {
    if (isExcludedPath(path, true)) continue

    const locale = getLocaleFromPath(path)
    if (!locale) result[path] = {}
  }

  for (const path of paths) {
    if (isExcludedPath(path, true)) continue

    const locale = getLocaleFromPath(path)
    if (!locale) continue

    const rootPath = path.replace(new RegExp(`^/${locale}`), '') || '/'

    result[rootPath][locale] = path
  }

  return result
}

function buildLinkWithAlternates (pathsObject, rootPath, completePath) {
  let message = ''

  message += indentation + '<url>\n'
  message += indentation + indentation + '<loc>https://joinpeertube.org' + completePath + '</loc>\n'
  message += indentation + indentation + '<xhtml:link rel="alternate" hreflang="en" href="https://joinpeertube.org' + rootPath + '" />\n'

  for (const locale of Object.keys(pathsObject[rootPath])) {
    // Already used en for the default page URL
    if (locale === 'en') continue

    const localePath = pathsObject[rootPath][locale]
    message += indentation + indentation + '<xhtml:link rel="alternate" hreflang="' + snakeToKebabCase(locale) + '" href="https://joinpeertube.org' + localePath + '" />\n'
  }

  message += indentation + '</url>\n\n'

  return message
}

function snakeToKebabCase (str) {
  return str.replace('_', '-')
}
