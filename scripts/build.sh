#!/bin/sh

set -eu

rm -rf ./dist

npm run build:news-meta

./node_modules/.bin/vite build --ssrManifest --outDir dist/static

./node_modules/.bin/vite build --outDir dist/server --ssr src/entry-server.js

node ./scripts/generate-sitemap.js > public/sitemap.xml
node ./scripts/prerender.js

npm run build:rss

rm -rf ./dist/server

mv dist/static/* ./dist
rmdir dist/static/
