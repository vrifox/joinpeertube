import { join, dirname } from 'path'
import { writeJsonSync } from 'fs-extra/esm'
import { fileURLToPath } from 'url'
import { getFreshNewsArticles, getArchiveNewsArticles } from './utils.js'

const __dirname = dirname(fileURLToPath(import.meta.url))

function getNewsMeta (lang) {
  return getFreshNewsArticles(lang).concat(getArchiveNewsArticles(lang))
    .map(a => ({
      id: a.id,
      title: a.title,
      shortDescription: a.shortDescription,
      image: a.image,
      isArchive: a.isArchive
    }))
}

function regenerateMeta () {
  const en = getNewsMeta('en')
  const fr = getNewsMeta('fr')
  const json = { en, fr }

  const outputPath = join(__dirname, 'meta.json')
  writeJsonSync(outputPath, json)

  console.log('Generated archives meta in %s', outputPath)
}

regenerateMeta()
