---
id: joinpeertube-2023
title: JoinPeertube fait peau neuve, la v5 PeerTube s'approche !
date: November 29, 2022
---

Bonjour,

Tout d'abord, PeerTube v5 est en route, car nous venons de publier une version RC (Realease Candidate) qui sera testée dans les prochaines semaines. Nous sommes impatients de vous présenter les améliorations et les nouvelles fonctionnalités !

En attendant, nous venons de publier un tout nouveau design de [JoinPeerTube](https://joinpeertube.org). Il a fallu beaucoup de travail pour simplifier ce site. Notre intention est d'en faire un outil accueillant, une sorte de porte d'entrée dans l'univers PeerTube accueillant tout le monde, en particulier pour les personnes qui ne sont pas férues de technologie.

  * Visite guidée du [nouveau JoinPeerTube](https://framablog.org/?p=29158) (coulisses incluses) sur notre blog ;
  * Découvrez [le tout nouveau site JoinPeerTube.org](https://joinpeertube.org) (et partagez-le autour de vous !) ;
  * Soutenez PeerTube en [soutenant Framasoft](https://soutenir.framasoft.org/fr/) (c'est nous !), et toutes nos actions.

![capture d'écran de l'en-tête du nouveau joinpeertube.org](https://framablog.org/wp-content/uploads/2022/11/Capture-du-2022-11-28-09-28-26.png)

Nous espérons vraiment que ce nouveau site Web aidera les créatrices de contenu (et les amateurs de contenu) à mieux comprendre ce qu'est PeerTube et ce qu'il n'est pas, ainsi que comment se lancer.

Framasoft est une petite association française à but non lucratif financée exclusivement par des dons (87 % de notre budget est constitué de dons des citoyens). Nous ne savons pas vraiment comment faire de la comm et du marketing, surtout en anglais. Ainsi, la plupart de nos fonds (qui financent le développement de PeerTube, entre autres) proviennent du public francophone.

Aujourd'hui, nous avons atteint 27% des objectifs de notre campagne de dons pour 2022. Nous avons besoin de vous pour nous aider à faire passer le message dans la communauté non francophone que tous nos projets ont besoin de soutien ! Si vous voulez nous aider à financer PeerTube et de nombreux autres projets en 2023, pensez à [soutenir Framasoft](https://soutenir.framasoft.org/fr/).

Nous espérons que vous apprécierez et partagerez ce nouveau [JoinPeerTube.org](https://joinpeertube.org),
Framasoft
