---
id: release-4.3
title: La 4.3 de PeerTube est sortie !
date: September 21, 2022
---

Bonjour à toutes et à tous,

Ça y est, la version 4.3 de PeerTube est sortie ! Découvrez son lot de nouvelles fonctionnalités : import automatique des vidéos d'une chaîne distante, plusieurs améliorations de l'interface ainsi que de l'intégration des vidéos en direct, des ajouts aux outils d'administration et bien d'autres choses encore... Petit tour des nouveautés !

#### Import automatique des vidéos d'une chaîne distante

C'est **LA** fonctionnalité que vous attendiez toustes : la possibilité d'importer automatiquement toutes les vidéos publiées d'une chaîne distante (provenant d'une autre plateforme vidéo) dans l'une de vos chaînes PeerTube. Vraiment pratique quand on est vidéaste publiant sur plusieurs plateformes et qu'on souhaite rendre visibles ses chaînes sur PeerTube. Ça peut aussi être intéressant pour regrouper dans une même chaîne les vidéos de plusieurs chaînes distantes. Un grand merci à [Florent](https://github.com/fflorent), l'un des administrateurs de [l'instance PeerTube Skeptikón](https://skeptikon.fr/), d'avoir développé cette nouvelle fonctionnalité.

Pour ajouter la synchronisation d'une chaîne distante, il suffit, une fois que l'administrateur a activé la fonctionnalité, de cliquer sur le bouton *Ajouter une synchronisation* proposé dans *Ma bibliothèque* / onglet *Chaînes* / bouton *Mes synchronisations* et de compléter les éléments demandés :

  * *URL de la chaîne distante* : l'url de la chaîne que l'on souhaite synchroniser
  * *Chaîne vidéo* : choisir dans le menu déroulant la chaîne avec laquelle on souhaite synchroniser

On vous propose ensuite 2 options de synchronisation :

  * *Importer tout et surveiller les nouvelles publications* : importer automatiquement toutes les nouvelles vidéos de la chaîne distante et toutes les vidéos déjà publiées
  * *Seulement surveiller les nouvelles publications* : paramétrer seulement l'import automatique de toutes les nouvelles vidéos de la chaîne distante

![](/img/news/release-4.3/fr/FR-new-synchronization.jpg)

Une fois cette synchronisation créée, PeerTube surveillera les nouvelles publications de la chaîne distante et importera automatiquement les nouvelles vidéos qui y seront publiées dans la votre.

Vous pouvez aussi utiliser cet outil de synchronisation pour importer les contenus d'une playlist distante dans l'une de vos chaînes.


#### Améliorations de l'interface de PeerTube

Afin que l'interface de PeerTube soit toujours plus facile et agréable à utiliser, nous avons sollicité une designer de la [Coopérative des Internets](https://www.lacooperativedesinternets.fr/). Nous avons donc intégré dans cette version 4.3 certaines de ses suggestions.

Nous avons revu le parcours proposé sur la page de création de compte afin de rendre plus explicites certaines informations à connaître avant de s'inscrire sur une instance :
  1. on vous explique pourquoi vous créer un compte
  2. on vous affiche les conditions d'utilisation et on vous invite à les accepter
  3. on vous demande d'indiquer les éléments nécessaires à la création de votre compte
  4. on vous propose de créer votre première chaîne (mais vous pouvez passer cette étape)
  5. votre compte est créé

![](/img/news/release-4.3/fr/FR-subscribe-compare.jpg "à gauche l'ancienne page de création de compte et, à droite, la nouvelle")

Nous avons aussi revu le positionnement des éléments de la page de connexion afin de placer en haut le message d'information.
Afin qu'elle soit plus visible, nous avons centré la barre de recherche située dans le haut de chaque page.
Enfin, nous avons augmenté la taille et éclairci la couleur de la police par défaut pour qu'elle soit plus accessible. Ces petites modifications vous apporteront davantage de confort dans l'utilisation de PeerTube.

#### Une meilleure intégration des vidéos et directs

Vous le savez sûrement, il est possible de permettre la lecture de vos vidéos et directs PeerTube depuis d'autres sites web via un système d'intégration (un petit code html que l'on récupère en cliquant sur le bouton *Partager* / onglet *Intégration* et qu'on copie dans une autre page web). Bien pratique, non ?

Mais jusqu'à maintenant, nous avions un petit souci d'affichage dans le cas de l'intégration de vidéos en direct : avant et après l'horaire du direct, le lecteur intégré n'affichait rien et donnait l'impression qu'il ne fonctionnait pas. Pour remédier à cela, nous avons ajouté des textes informatifs qui s'affichent avant que le direct soit lancé et une fois qu'il est terminé.

Nous avons aussi ajouté aux vidéos intégrées la lecture automatique au moment où le direct débute. Il n'est donc plus nécessaire d'actualiser la page web régulièrement. Précisons tout de même que certains navigateurs sont paramétrés par défaut pour bloquer la lecture automatique des vidéos et qu'il vous faudra modifier vos paramètres pour bénéficier de cette nouvelle fonctionnalité.


#### Améliorations de l'administration d'une instance

Afin que les personnes qui administrent une instance PeerTube puissent davantage paramétrer les usages au sein de leur instance, nous leur permettons désormais de :

* **réaliser des actions en lot sur les instances fédérées**
La possibilité de réaliser des actions en lot (sélection de plusieurs items afin de leur appliquer tous le même traitement) était déjà disponible pour les pages de l'onglet *Aperçu* (utilisateurs, vidéos et commentaires). Elle l'est désormais pour les pages de l'onglet *Fédération*. Bien pratique pour supprimer plusieurs abonnements à la fois ou pour retirer la permission à certaines instances de se fédérer avec la vôtre.

* **désactiver le transcodage de la résolution originale de la vidéo téléversée ou du flux en direct**
En tant qu'administrateur⋅ice d'une instance, il vous est possible de paramétrer les définitions dans lesquelles les vidéos téléversées sont encodées. Jusqu'à maintenant, PeerTube encodait systématiquement le fichier de la vidéo originale, même si sa définition n'était pas activée. Cela encombrait fortement certaines instances. Il est donc désormais possible de désactiver l'encodage de la vidéo originale lorsque sa définition est supérieure à la définition maximale paramétrée. Pour cela, il vous suffit de vous rendre dans le menu *Administration*, onglet *Configuration* puis *Transcodage de la VOD* et de cocher la case devant le texte *Toujours transcoder la résolution originale*.

* **supprimer un fichier vidéo spécifique**
Parce qu'on stocke souvent plusieurs versions d'une vidéo et que cela peut vite faire augmenter les besoins de stockage de son instance, vous pouvez désormais supprimer spécifiquement certains fichiers. Cela peut être très utile si par exemple, vous voulez supprimer tous les fichiers vidéo dont la définition est supérieure à celle que vous avez définie pour votre instance.

![](/img/news/release-4.3/fr/FR-suppression-video-specifique.jpg)


#### Et aussi
PeerTube est désormais accessible dans 2 nouvelles langues : le [Toki Pona](https://fr.wikipedia.org/wiki/Toki_pona) et le Croate !

Nous avons réalisé plusieurs optimisations pour que le logiciel tienne mieux la charge. Ses performances s'en voient donc grandement améliorées.

Beaucoup d'autres améliorations ont été apportées dans cette nouvelle version. Vous pouvez voir la liste complète (en anglais) sur https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md.

Enfin, nous vous rappelons que nous avons lancé en juillet un outil pour récolter vos besoins sur PeerTube. En effet, nous avons besoin de savoir ce qui manque aux créateurs de contenu, aux amateurs de vidéo et aux personnes non-tech. Quelles fonctionnalités pourrions-nous développer pour rendre l'usage de PeerTube encore plus simple et agréable ? Nous vous invitons à vous exprimer (en anglais) sur [Let’s improve PeerTube!](https://ideas.joinpeertube.org/). Et si vous n'êtes pas forcément inspiré⋅es, vous pouvez toujours voter pour l'une des 80 propositions déjà postées. On en profite pour remercier toutes les personnes à l'origine de ces propositions et toutes celles qui sont allées voter.

Merci à toustes les contributeur⋅ices de PeerTube !
Framasoft
