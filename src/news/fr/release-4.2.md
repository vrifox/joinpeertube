---
id: release-4.2
title: La 4.2 de PeerTube est sortie !
date: June 7, 2022
---

Bonjour à toutes et à tous,

Ça y est, la version 4.2 de PeerTube est sortie ! Découvrez son lot de nouvelles fonctionnalités : la modification des vidéos depuis l'interface web, des statistiques de visionnage détaillées pour les vidéos, la possibilité de régler la latence lors d'une diffusion en direct et bien d'autres choses encore... Petit tour des nouveautés !


#### Modifier ses vidéos depuis l'interface web de PeerTube

Voilà une nouvelle fonctionnalité qui va enchanter les vidéastes en herbe ! Jusqu'à maintenant, PeerTube ne proposait aucune option pour l'édition vidéo : les vidéastes devaient réaliser le travail de montage avant de publier sur PeerTube. Désormais, si vous avez besoin de faire quelques opérations de montage, vous pouvez le faire directement dans l'interface web.

Après avoir mis en ligne votre vidéo originale, il vous suffit de cliquer sur le menu *...* et de choisir *Studio*. Vous pouvez alors choisir de :
 * couper la vidéo (en précisant un nouveau time-code de début et/ou de fin)
 * ajouter une intro au début et/ou une outro à la fin de la vidéo (en chargeant un fichier vidéo)
 * ajouter une icône/un filigrane dans le coin supérieur droit de la vidéo (en chargeant une image)

Une fois ces modifications paramétrées, PeerTube ré-encodera automatiquement la nouvelle vidéo et mettra à jour la vidéo originale.

![](/img/news/release-4.2/fr/FR-studio.png)

Cette fonctionnalité a été développée grâce au soutien financier de la Direction du numérique du Ministère de l'Éducation Nationale, de la Jeunesse et des Sports. Elle répond à un besoin de l'équipe en charge de la plateforme [apps.education.fr](https://apps.education.fr/), laquelle offre aux agentes et agents de l'Éducation Nationale des outils (libres !) pour travailler à distance. Merci à elleux !

#### Des statistiques sur les vidéos

PeerTube ne proposait que très peu de métriques sur les vidéos : un compteur de vues et un compteur de *j'aime*. La version 4.2 vous fournit, pour chaque vidéo, de nouveaux compteurs :
  * durée moyenne et totale de visionnage
  * nombre maximum de spectateur⋅ices en simultané
  * nombre de pays de provenance des spectateur⋅ices

Pour y accéder, c'est via le menu `...` situé sous le lecteur vidéo (à droite) en choisissant *Statistiques*.

![](/img/news/release-4.2/fr/FR-stats.png)

En plus de ces compteurs, PeerTube vous permet de visualiser ces données via des graphiques :
  * le nombre de spectateur⋅ices
  * la durée des visionnages
  * la répartition des vues par pays d'origine (si les administrateur⋅ices de l'instance n'ont pas désactivé cette option)
  * le taux de rétention de l'audience (pour identifier les moments de vos vidéos qui ont su capter l'attention de l'audience)

Ce groupe de fonctionnalités a été développé grâce au soutien financier de *HowlRound Theatre Commons* à *Emerson College*. Merci à elleux !

#### Sauvegarder chaque direct d'un *live* permanent/récurrent

Vous le savez sûrement déjà, PeerTube vous propose 2 façons de diffuser en direct :
* le *live* "normal" : on ne peut diffuser qu'une seule fois à partir de l'url
* le *live* permanent/récurrent : on utilise la même url pour tous les directs

Jusqu'à maintenant, seul le *live* "normal" permettait de publier le replay de la vidéo sur la même url que celle du *live*. Losqu'on fonctionnait en *live* permanent/récurrent, il n'était pas possible d'enregistrer automatiquement les replays dans PeerTube. Il fallait utiliser un outil externe pour chaque enregistrement et les publier manuellement dans PeerTube, comme n'importe quelle vidéo.

Désormais, il est possible de sauvegarder chaque direct d'un *live* permanent/récurrent sur une nouvelle url. Pratique, non ? En plus, la vidéo publiée se voit dotée automatiquement de l'ensemble des informations descriptives du *live* permanent/récurrent pour une meilleure indexation.

![](/img/news/release-4.2/fr/FR-rediff-automatique-live.png)

Cette fonctionnalité a elle aussi été développée grâce au soutien financier de *HowlRound Theatre Commons* à *Emerson College*. Merci à elleux !

#### Régler la latence pour les lives / directs

Aujourd'hui, lorsque vous diffusez en direct sur PeerTube, on estime que la latence (délai entre le moment où le flux vidéo est envoyé et celui où la vidéo est visionnable) est de 30 à 40 secondes. Cela s'explique du fait que PeerTube utilise un protocole pair-à-pair (P2P) pour diffuser les vidéos, ce qui permet d'alléger la charge des sites web qui les hébergent.

Afin de modifier ce temps de latence, nous proposons aux vidéastes 2 nouveaux réglages :
  * réduire la latence en désactivant complètement le P2P
  * augmenter la latence, ce qui permettra d'échanger plus efficacement des segments de la vidéo en P2P.

Pour modifier ce réglage, il vous suffit d'éditer les informations du live. Dans l'onglet *Paramètres du direct*, vous pouvez choisir le mode de latence souhaité.

![](/img/news/release-4.2/fr/FR-latence.jpg)

#### Et aussi

Grâce à la [contribution de @lutangar](https://github.com/Chocobozzz/PeerTube/pull/4666), il est dorénavant très facile d'**éditer les sous-titres d'une vidéo directement depuis l'interface d'édition** de chaque vidéo.

PeerTube permet désormais aux administrateur⋅ices d'une instance **d'activer/désactiver l'affichage de l'avatar du propriétaire d'une vidéo** sur les différentes pages qui listent les vidéos (*Vidéos locales* par exemple).

Beaucoup d'autres améliorations ont été apportées dans cette nouvelle version. Vous pouvez voir la liste complète (en anglais) sur [https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md).

Merci à toustes les contributeur⋅ices de PeerTube !
Framasoft
