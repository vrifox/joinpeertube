---
id: release-5.0
title: Apprenez tout sur PeerTube v5 !
date: December 13, 2022
---

🎉 La v5 de PeerTube v5 est désormais en ligne 🎉.

À cette occasion, nous avons publié [un billet de blog](https://framablog.org/2022/12/13/peertube-v5-le-resultat-de-5-ans-de-travail-artisanal/) résumant une année de travail et d'améliorations sur l'écosystème PeerTube. 

Il présente également les nouvelles fonctionnalités de PeerTube v5, et ce sur quoi nous pensons pouvoir travailler l'année prochaine.

  * Découvrez ces nouvelles fonctionnalités [sur notre blog](https://framablog.org/2022/12/13/peertube-v5-le-resultat-de-5-ans-de-travail-artisanal/) ;
  * Consultez le [code de la v5](https://github.com/Chocobozzz/PeerTube/releases/tag/v5.0.0) par vous-même ;
  * Proposez vos idées (et votez pour les idées qui vous enthousiasment) sur [Let’s Improve PeerTube](https://ideas.joinpeertube.org/) (en anglais uniquement)
  * Soutenez PeerTube en [soutenant Framasoft](https://soutenir.framasoft.org/), et découvrez toutes nos actions.

![Illustration CC BY David Revoy](https://framablog.org/wp-content/uploads/2022/12/Peertube-v5_by-David-Revoy.jpg)

Nous tenons à remercier toutes les personnes qui ont contribué à l'écosystème PeerTube tout au long de l'année : vous êtes toutes et tous formidables ! Cette année, certaines fonctionnalités ont été financées par des structures partenaires de PeerTube (voir l'article blog). Mises bout à bout, leurs contributions représentent une petite moitié de notre budget annuel consacré à PeerTube (que nous estimons à 70 000 €). Le reste de l’argent provient donc du budget de Framasoft, c’est-à-dire des dons que notre association reçoit de son public principalement francophone. Nous avons donc besoin de votre aide. Faites marcher le bouche-à-oreille pour faire connaître notre [campagne de dons](https://soutenir.framasoft.org/) autour de vous.

Si vous voulez nous aider à financer notre travail en 2023 sur PeerTube et de nombreux autres projets, n'hésitez pas à partager cette adresse sur vos plateformes PeerTube et dans vos communautés qui bénéficient de cette alternative.

Nous espérons que vous apprécierez PeerTube v5,
Framasoft
