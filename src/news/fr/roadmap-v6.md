---
id: roadmap-v6
title: Ce que 2023 réserve à PeerTube...
date: February 8, 2023
---

Nous (Framasoft, une petite association à but non lucratif !) sommes fier·es de vous présenter notre feuille de route pour **les développements et les projets annexes que nous avons prévu pour PeerTube en 2023**. Cette feuille de route combine autant des avancées que nous avions envie d'apporter au logiciel que des propositions que vous nous avez transmises [ces derniers mois](https://ideas.joinpeertube.org/).

Il nous semble important de repréciser que nous ne dédions qu’un développeur à PeerTube (oui, un seul !), PeerTube étant un projet parmi plus d’une cinquantaine menés par notre association.

#### Fin février : PeerTube 5.1

Nous prévoyons de publier la version 5.1 fin février. Voici les nouveautés envisagées :

  * Gestion facilité des demandes de création de compte (avec l’approbation de l’administrateur)
  * Les développeurs pourront profiter d'une **amélioration de l’API pour les plugins d’authentification externe** (définition d'un quota, mise à jour de l’utilisateur, etc.)
  * Optimiser la **récupération des commentaires** d'une vidéo
  * Ajout d'un **bouton « Reprendre le direct »** dans le lecteur vidéo
  * Amélioration et correction de bugs (notamment les bugs constatés lors [du test de charge de fin décembre](https://www.octopuce.fr/test-de-charge-dun-peertube-en-live-avec-auposte/)


![screenshot](/img/news/roadmap-v6/fr/screenshot-bouton-live.jpg)


#### Mai 2023 : PeerTube 5.2

C'est en mai que nous prévoyons de publier la version 5.2 de PeerTube, qui proposera la possibilité de **transcodage à distance** ([fonctionnalité qui a reçu de nombreux votes !](https://ideas.joinpeertube.org/posts/2/support-for-transcoding-by-remote-workers)). Cela permettra de réduire la puissance requise pour un serveur PeerTube en déléguant les tâches consommatrices à des machines extérieures.
Dans un premier temps, cela concernera uniquement les vidéos hébergées, mais sera conçu pour être évolutif (notamment pour les livestreams). Un sacré défi technique qui nous attend !


#### Novembre-Décembre 2023 : PeerTube v6

C'est en fin d'année que nous prévoyons de publier la prochaine version majeure de PeerTube, et les nouvelles fonctionnalités prévues sont toutes **inspirées de vos suggestions** sur [notre outil de feedback *Let’s Improve PeerTube*](https://ideas.joinpeertube.org/). Vous retrouverez :

  * [L'ajout de chapitres aux vidéos](https://ideas.joinpeertube.org/posts/6/add-chapters-to-my-videos)
  * [L'affichage d'une vignette d’aperçu dans la barre de progression](https://ideas.joinpeertube.org/posts/9/get-a-preview-thumbnail-in-video-progress-bar)
  * [La protection des vidéos avec un mot de passe/token](https://ideas.joinpeertube.org/posts/7/protect-video-viewing-with-a-password-token)
  * [La possibilité de téléverser une nouvelle version de sa vidéo](https://ideas.joinpeertube.org/posts/3/upload-a-new-version-of-my-video)


#### Mais aussi...

En ce début d'année, nous accueillons Wicklow dans l'équipe, pour un stage de 6 mois. C'est l'occasion d'épauler le développeur de PeerTube et de familiariser plus de personnes avec la base de code du logiciel.

Nous allons aussi bien sûr continuer à **corriger les bugs, nettoyer le code et améliorer l’architecture** du logiciel pour faciliter les différentes avancées.

Nous continuerons aussi à **soutenir le développement externe et communautaire**, tel que [le plugin Live-Chat](https://www.john-livingston.fr/foss/article/peertube-chat-plugin-quick-feedbacks-about-a-live-stress-test).

Enfin, nous allons travailler sur de la **curation de contenus** pour notre plateforme vitrine [Peer.tube](https://peer.tube/) (oui, avec un point au milieu !), pour permettre de présenter [une porte d'entrée à PeerTube](https://framablog.org/2022/12/06/framasoft-2022-une-tambouille-mijotee-grace-a-vous-grace-a-vos-dons/#peer.tube), qui ressemble à Framasoft .

![](/img/news/roadmap-v6/fr/Peertube-v5_by-David-Revoy.jpg)

PeerTube, comme l’ensemble de nos projets, est **principalement financé par les dons à notre association**. Sur cette feuille de route, seule la fonctionnalité de transcodage à distance de la v5.2 est déjà financée, grâce à un don de la fondation [NLnet](https://nlnet.nl/).

Vous voulez nous aider à réaliser cette feuille de route ? Vous pouvez nous soutenir **en contribuant à PeerTube**, **en partageant cette information** et (si vous pouvez vous le permettre) [en faisant un don à Framasoft](https://support.joinpeertube.org/fr/).

Merci d'avance de votre soutien !
Framasoft
