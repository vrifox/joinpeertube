---
id: isd-study
title: A statement about the German ISD study on PeerTube
date: December 21, 2022
---

We, Framasoft, have been developing the PeerTube software for 5+ years.

[Framasoft](https://framasoft.org) is a French non-profit of 38 members (10 employees, 28 volunteers), PeerTube is one of our 50+ projects, and we do all this work with only one developer (who is not event full time on PeerTube). Please note that maybe 2 or 3 among us understand, more or less fluently, German.

On dec. 19th, a journalist from [Tagesspiegel Background](https://background.tagesspiegel.de/) informed us that a study by German researchers on the use of PeerTube by right-wing extremists was about to be published. He asked us 3 questions about the scope of the problem and what could be done, and we answered him that day.

On dec. 20th, we have been able to get this study (it is [available online here](https://www.isdglobal.org/isd-publications/die-hydra-im-netz-herausforderung-der-extremistischen-nutzung-des-fediverse-am-beispiel-peertube/)) and translate it. This is why it took us time to write and publish a collective statement.

First of all, we would like to thank the researchers of the Institute for Strategic Dialogue for their work. The more we will gain knowledge of how disinformation manipulators and right-wing extremists use PeerTube, the more the PeerTube communities will learn how to effectively protect themselves from such content.

Shared knowledge is shared power.


#### PeerTube says no to fascists and conspiracy manipulators

Let's be crystal clear: Framasoft's values are fundamentally opposed to right-wing extremism. This is also true for conspiratorial manipulations that lead to hurting and killing people ([here is our recently published manifest](https://framasoft.org/en/manifest/) stating our core values - Warning: may be poorly translated from French).

We agree with the results of the study. In our experience, right-wing extremists represent a very, very small share of the PeerTube federation (called the "vidiverse"), but they know how to be very loud, energy consuming trolls.

We should not ignore the fact that, in 2022, if any society is producing fascist and Nazi groups, it is not because of technology, but because of a deeper problem, which is mainly related to the complacency of some governments with extreme right-wing ideas. Nevertheless, it is also true that some technical devices that work on the basis of audience measurement and advertising are the first to disseminate extremist ideas. The Fediverse does not work like that, but we should work to keep it healthy.

PeerTube is free software, we cannot prevent these people from using it. Germany, France and most modern democracies have already introduced laws that can lead Justice to condemn PeerTube administrators who knowingly and willingly host hateful and Nazi content.

What we can do (and [have been doing](https://joinpeertube.org/faq#does-peertube-offer-moderation-tools) and are willing to continue) is giving PeerTube communities the tools to moderate, protect themselves from and ostracize right-wing extremists and harmful PeerTube platforms.

We "moderate" (read: ban) fascist content from any tool we manage. Thus, we regularly clean the [instance index](https://instances.joinpeertube.org/instances). For example we removed from the index problematic instances, several of which are German. This does not prevent these instances from existing, but at least we try to keep the index healthy.


#### We need the communities to step up

The study clearly states it: one of the major solutions to such content relies in empowering the communities.

[PeerTube-isolation](https://peertube\_isolation.frama.io/) is a blocklist that is maintained in total independence from us. [Installing their plugin](https://framagit.org/framasoft/peertube/official-plugins/-/blob/master/peertube-plugin-auto-mute/README.md) on your PeerTube platform can help you make sure you won't federate with dangerous content.

We encourage anyone who wants to help to contribute to the PeerTube-Isolation community work. We automatically pull out the instances they block from the index that feeds [SepiaSearch](https://sepiasearch.org/), our PeerTube Search Engine.

Our goal is to keep harmful and hateful content isolated in their own federation bubble, where they can be as loud as they want. Thus they won't contaminate others with their dangerous filth. Thus they'll understand they are not welcomed in the PeerTube vidiverse, and they won't have any interest trying to invest it.

In addition, we are always exploring new ways to help PeerTube communities and administrators moderate hate speech and harmful content. If you can think of a feature we could add to PeerTube to help isolating such filth, please share it on [Let's Improve PeerTube](https://ideas.joinpeertube.org/).


#### A call to (help) PeerTube instance administrators

That being said, a tool can only do so much.

Instance administrators need help, because they have several tasks to fulfill: system administration (backup, updates, etc.), content curation (do I want to host edutainment videos? original fictions? videos by Queer creators?), federation policies (which platforms do I want mine to follow? which follows do I accept?), moderation policies (and moderation can be needed in the federation choices, content hosted, comments, etc.)

It is a very complex job, and usually you should not to be alone to complete it. It is a crucial job, though. We need instance administrators to have rules about the platform they federate with, so they can enforce their own policies and - hopefully- stop the dissemination of harmful and dangerous content. But it is a lot of work, so it requires a lot of help.

If you want to help, try to join a PeerTube administration team. Offer them help. Report problematic content. If public organizations can provide training, help, and tools, it can always be useful ! But PeerTube is a [Commons](https://en.wikipedia.org/wiki/Commons): we cannot address such an issue unless we work together as a (diverse and plural) community.


#### Shared knowledge is shared power

The PeerTube communities sure need more shared knowledge on right-wing extremists, their content, their methods, their arguments \& fallacies, and how to protect ourselves from it.

But we also need more people helping and taking charge, together, as communities, because we are very invested in keeping PeerTube ecosystem healthy.
