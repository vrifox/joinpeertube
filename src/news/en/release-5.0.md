---
id: release-5.0
title: Learn all about PeerTube v5!
date: December 13, 2022
image: https://framablog.org/wp-content/uploads/2022/12/Peertube-v5_by-David-Revoy.jpg
---

🎉 PeerTube v5 is now available 🎉.

For this occasion, we have published [a blog post](https://framablog.org/2022/12/13/peertube-v5-the-result-of-5-years-handcrafting/) summing up a year of hard work and improvements on the PeerTube ecosystem.

It also presents the new features of PeerTube v5, and what we feel we might work on next year.

  * Learn all about those empowering new features [on our blog](https://framablog.org/2022/12/13/peertube-v5-the-result-of-5-years-handcrafting/);
  * See the [code of the V5](https://github.com/Chocobozzz/PeerTube/releases/tag/v5.0.0) by yourself;
  * Publish your ideas for PeerTube V6 on [Let’s Improve PeerTube](https://ideas.joinpeertube.org/) (and vote for the ones you’re excited about)
  * Support PeerTube by [supporting Framasoft](https://soutenir.framasoft.org/en), and find out about all our actions.

![Illustration CC BY David Revoy](https://framablog.org/wp-content/uploads/2022/12/Peertube-v5_by-David-Revoy.jpg)

We would like to thank every person who has contributed to the PeerTube ecosystem throughout the year: y'all are amazing! This year, some features have been directly funded by PeerTube-supporting organizations (see our blogpost). Put together, their contributions represent a small half of our annual budget dedicated to PeerTube (which we estimate at 70 000 €). The other part comes from Framasoft’s budget, i.e. from the donations that our non profit receives from its mainly French-speaking community. To us, it seems almost unfair that it is mainly French speakers who finance a tool that has a truly international scope… So we need your help. Spread the word about our [donation campaign](https://supprot.framasoft.org) around you, especially outside the French-speaking world.

If you want to help us fund our work in 2023 on PeerTube v6 and many other projects, please consider supporting Framasoft, share the address [support.framasoft.org](https://support.framasoft.org) on your PeerTube platforms, in your communities that benefit from this alternative. 

We hope you will enjoy PeerTube v5,
Framasoft
