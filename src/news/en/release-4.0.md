---
id: release-candidate-4.0
title: Learn all about PeerTube v4!
date: November 30, 2021
---

Hello,

PeerTube v4 is on its way, as we have just published a release candidate version that will be tested out in the next few weeks.

For this occasion, we have published a blog post summing up a year of hard work and improvements on the PeerTube ecosystem. It also presents the new features of PeerTube v4, and what we feel we might work on next year.

* Learn all about those empowering new features [on our blog](https://framablog.org/2021/11/30/peertube-v4-more-power-to-help-you-present-your-videos/);
* See the [code of the release candidate v4](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md#v400-rc1) by yourself;
* Support PeerTube by [supporting Framasoft](https://soutenir.framasoft.org/en), and find out about all our actions.

![](/img/news/release-4.0/en/peertube-live.jpg)

We would like to thank every person who has contributed to the PeerTube ecosystem throughout the year: y'all are amazing! Our special thanks also goes to NLnet, whose 50 000 € grant funded about two thirds of our 2021 spendings on the PeerTube project. The remaining third came directly from our not-for-profit budget, that comes from grassroots donations (usually from our French audience) we get for all our actions.

If you want to help us fund our work in 2022 on PeerTube and many other projects, please consider [supporting Framasoft](https://soutenir.framasoft.org/en). And don't forget to share if you care!

We hope you will enjoy PeerTube v4,
Framasoft
