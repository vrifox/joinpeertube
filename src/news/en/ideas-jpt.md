---
id: ideas-jpt
title:  Let's improve PeerTube - Help us define PeerTube's future roadmap
date: July 21, 2022
---

*Bonjour* everyone!

We need your help to share and contribute to a new feedback tool: [Ideas.joinpeertube.org](https://ideas.joinpeertube.org).


#### Five years of building the ecosystem of PeerTube

By the end of 2022, PeerTube will be five years in the making! In the last four years, **with only one (not even full time!) paid developer**, we got:

* From a POC to a fully operative federated video platform with p2p broadcasting, complete with subtitles, redundancy, video import, search tools and localization ([PeerTube v1, oct. 2018](https://framablog.org/2018/10/15/peertube-1-0-the-free-libre-and-federated-video-platform/))
* Notifications, playlists, a plugin system, moderation tools, federation tools, a better video player, a presentation website and an instances index ([PeerTube v2, nov. 2019](https://framablog.org/2019/11/12/peertube-has-worked-twice-as-hard-to-free-your-videos-from-youtube/))
* Federated research tool (and a search engine), more moderation tools, lots of code improvement, UX revamping, and last but not least: p2p livestream ([PeerTube v3, jan. 2021](https://framablog.org/2021/01/07/peertube-v3-its-a-live-a-liiiiive/))
* Improved transcoding, channels and instances homepage customization, improved research, an even better video player, filtering videos on pages, advance administration and moderation tools, new video management tool, and a big code cleaning session ([PeerTube v4, dec. 2021](https://framablog.org/2021/11/30/peertube-v4-more-power-to-help-you-present-your-videos/))

All of this, in 4 years, with only one employed developer, and financed through donations (please consider [supporting us at Framasoft](https://soutenir.framasoft.org/en/)!).

This year has already brought many improvements, and we know what we want to do for the v5 by the end of 2022, but... what next?

![Sepia with a jetpack and a toolbox](https://joinpeertube.org/img/news/roadmap-v3-part-2/en/2020-05-21_Peertube-Plugin_by-David-Revoy%20lowres.jpg)

#### Ideas for PeerTube: let's get non-tech-savvy people's feedback

Until now, developers, admins and tech-savvy people could suggest improvements and new features for PeerTube by publishing and commenting issues in the git repository.

Nowadays, PeerTube is gaining momentum and users. It is getting out of the "experts" bubble, and that's a great opportunity. So **we need to know what content creators, video-lovers and non-tech-savvy people miss from PeerTube**, what changes or new experiences they would like to get.

We need more diverse feedback, so we want to propose a user-friendly feedback tool:

**Here is our feedback tool:** [Ideas.Joinpeertube.org](https://ideas.joinpeertube.org).

* If you know what would improve your PeerTube experience, **please add an idea**.
* If you see anything in there that catches your eye, **please vote for it**.
* If you know people who would like more from PeerTube, **please share this feedback tool**.

![screenshot of ideas.joinpeertube.org](/img/news/ideas-jpt/en/screen-fider.png)

#### We're but a small non-profit

We had to compromise: [Ideas.Joinpeertube.org](https://ideas.joinpeertube.org/) will only be in English, as we don't have enough humans, talents and energy to maintain a multi-languages feedback tool.

Because at Framasoft (that's the name of our association!), we still are a small French non-profit (10 employees, 36 members) funded through [donations](https://soutenir.framasoft.org). **PeerTube is one of our 50+ projects** (yes, it's a big one, but still one among many).

We need to collect feedback not only for ourselves, but for everyone who would like to contribute to PeerTube ecosystem (core code, plugins, documentation, indexes, moderation lists, etc.).

By displaying to the world what the PeerTube community wants from PeerTube, we will be able (late 2022 - early 2023) to choose what to add to the roadmap for v6, as long as it's within what we are able and willing to do.

But **we also hope that other parties will be interested in contributing to PeerTube** by developing awaited and upvoted ideas.

[![Click to support Framasoft](/img/news/ideas-jpt/en/soutenir.jpg)](https://soutenir.framasoft.org)

#### Contribute and share!

Now this tool is in your hands. We trust you will contribute by sharing ideas to improve PeerTube, voting for the ideas you like and sharing this tool to concerned people around you.

As we don't have (and can't afford, and don't want) big marketing tools and communication campaigns, **the success of this tool (and of PeerTube) lays exclusively in your hands**. We know those are capable hands.

Thanks to all in advance for sharing and caring,
Framasoft
