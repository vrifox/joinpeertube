import { writeFileSync } from 'fs'
import { Feed } from 'feed'
import { getFreshNewsArticles, getArchiveNewsArticles } from './utils.js'
import { markdownIt } from './news-markdown-it.js'

const options = {
  title: 'PeerTube news!',
  description: 'Discover the latest PeerTube improvements',
  link: 'https://joinpeertube.org',
  image: 'https://joinpeertube.org/img/card-opengraph.jpg',
  favicon: 'https://joinpeertube.org/img/icons/favicon.png',
  copyright: 'PeerTube news! content © 2021, Framasoft, licenced under CC-BY-SA 4.0',
  updated: new Date(),
  generator: 'Framasoft',
  author: {
    name: 'Framasoft',
    email: 'contact@framasoft.org',
    link: 'https://contact.framasoft.org'
  }
}

function regenerateRSS (language) {
  const feed = new Feed({
    title: options.title,
    description: options.description,
    id: options.link,
    link: options.link,
    language,
    image: options.image,
    favicon: options.favicon,
    copyright: options.copyright,
    updated: options.updated,
    generator: options.generator,
    author: options.author
  })

  getArticles(language).forEach(article => {
    feed.addItem({
      title: article.title,
      id: `${options.link}/news#${article.id}`,
      link: `${options.link}/news#${article.id}`,
      content: markdownIt.render(article.body),
      date: new Date(article.date)
    })
  })

  const outputPath = 'dist/static/rss-' + language + '.xml'
  writeFileSync(outputPath, feed.rss2())

  console.log('RSS feed generated in %s.', outputPath)
}

function getArticles (language) {
  return getFreshNewsArticles(language)
    .concat(getArchiveNewsArticles(language))
    .sort((a, b) => {
      if (a.date < b.date) return 1
      if (a.date > b.date) return -1
      return 0
    })
}

regenerateRSS('en')
regenerateRSS('fr')
