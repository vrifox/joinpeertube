---
id: release-3.2
title: PeerTube v3.2 is out!
date: May 27, 2021
---

Hi everybody,

2 months ago, we published PeerTube v.3.1! Since then, we have made many improvements to your favourite tool... So here are some details on what this version 3.2 brings you.

#### Improved differentiation of channels from accounts

During our work with designer Marie-Cécile Godwin, we noticed that it was not easy to differentiate accounts from channels. In order to make it easier for you to find your way around, we have redesigned the appearance of these two element. Channels' avatars (in square format) are now different from accounts' avatars (in circle format).

![img](/img/news/release-3.2/en/chaine-nvdesign-EN.png)

As you can see on this screenshot, it is now more obvious to identify that you are on a channel page (square avatar and "video channel" indication above the channel title). Thanks to the block placed on the right, you can identify which account manages this channel. If you are curious enough to click on the "view account" button, you will be redirected to a page that lists the account's channels. This page has been redesigned to make it easier to differentiate one channel from another.

We have also added new features to make channels more attractive. For example, you can add an illustration banner and a "support" button to show how people can help you to fund your video creations.

Finally, channels (rather than accounts) are now displayed first on video thumbnails. And we have increased the size of video miniatures by a third, so there are fewer videos displayed on a single line, which is more pleasant.

With these improvements, our hope is that more of you will create channels to post your videos.

#### New features to improve your use of PeerTube

This version 3.2 is also filled with small improvements and new features that will make your PeerTube experience more pleasant. Not all of them will revolutionize your practice, but we are still very happy to have implemented them.

  * First of all, PeerTube now automatically resumes videos for non logged-in users. If you are interrupted while watching, you won't need to look up where you were to continue watching the video.

  * PeerTube brings you more robust uploads by using a resumable upload endpoint. Actually, it was quite annoying to have to restart an upload from the beginning (especially if the video was big) when it was interrupted due to a faulty internet connection. You have been pointing this out for a while, and now this problem is history!

  * We have changed the default download setting. Previously, when you clicked on the "Download" button, the torrent download was set by default. Now it is direct download. But of course you can always change this setting.

  * When you have uploaded many videos on your account, it is sometimes not easy to find them. So we've added the ability to sort videos according to several criteria: publication date, most viewed, most liked, longest, etc. You can also display only your live broadcasts.

  * To facilitate instance administration, we have added a notification to notify administrators on new available PeerTube version and plugins updates. This way, we hope that more instances will be kept up to date.


#### Improvements on the video player context menu

Did you notice that when you right-click in the video player, you can display a contextual menu? This feature has been around for a while, but it wasn't highlighted. And since we've just improved this menu, this is a good opportunity to introduce this feature to those who haven't seen it yet!

![img](/img/news/release-3.2/en/menu-contextuel.png)

We added small icons to easily understand available actions. There is a new "stats for nerds" item which displays, as the name suggests, technical information that only the most experienced geeks will understand ;)

We have made many other improvements in this new version. You can read the whole list on [https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md).

Thanks to all PeerTube contributors!
Framasoft
