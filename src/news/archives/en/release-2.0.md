---
id: release-2-0-0
title: Release of PeerTube v2, and redesign of JoinPeertube
date: November 12, 2019
twitter: https://twitter.com/framasoft/status/1194173709998788609
---

Hello,

After a year of work and improvements, we have just released version 2 of PeerTube! Everything is explained in detail in the <a target="_blank" rel="noopener noreferrer" href="https://framablog.org/2019/11/12/peertube-has-worked-twice-as-hard-to-free-your-videos-from-youtube/">Framablog article</a> that we invite you to read.

We present, among other things:

- features and improvements since v1 of November 2018
- work to facilitate the federation and presentation of proceedings
- the new version of [JoinPeertube](https://joinpeertube.org), which is enriched by this work
- [PeerTube's official documentation site](https://docs.joinpeertube.org) (administration and use)
- our desires and projects for the future of PeerTube (we are considering a new crowdfunding, and dreaming of a video-remix tool and of live streaming!)

These improvements are the result of the many contributions of the members of the PeerTube community (thank you!), but also of a year of work that we have financed through [donations that support all of our association's projects](https://soutenir.framasoft.org), currently in a [donation campaign](https://contributopia.org/journal).

Freely,
Framasoft.
