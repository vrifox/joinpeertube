---
id: roadmap-v4
title: Our plans for PeerTube v4
date: April 13, 2021
---

<p>
  In the <a href="https://framablog.org/2021/01/07/peertube-v3-its-a-live-a-liiiiive/" target="_blank">PeerTube v3
    release blogpost</a>, we announced that we wouldn't resort to crowdfunding to finance PeerTube's development in
  2021.
</p>

<p>
  We are glad to announce that a big part of PeerTube v4 will be funded through an NLnet grant under their <a
    href="https://nlnet.nl/discovery/" target="_blank">"Search and discovery" program</a> (find <a
    href="https://nlnet.nl/project/PeerTube/" target="_blank">here</a> their page on PeerTube).
</p>

<p>
  We plan to develop PeerTube's v4 all along 2021, with our main focus on customizing the experience so that instance
  administrators, content creators and users can display and discover the videos to their liking.
</p>

<p>
  We truly believe that empowering instance administrators and content creators to customize their instances and
  channels is a great way forward to help PeerTube users to find and identify the videos they want to watch.
</p>

<p>
  Such customization will have a direct impact on search, and on search results. Moreover we plan to extend the search
  scope to playlists so they can be displayed in search results. We also plan to improve search result pages with a more
  ubiquitous sorting/filter system and a better interface.
</p>

<h4>PeerTube's v4 main features</h4>

<p>
  Here are the main "customization" features we plan to be working on:
</p>

<ul>
  <li>Sorting/filter system on pages that list videos</li>
  <li>Customize left menu for instances (through plugin API)</li>
  <li>Customize homepage for instances (through a markup system)</li>
  <li>Customize channels for content creators</li>
  <li>Incentives to complete instance/channel attributes (i.e. descriptions, images, terms of service, etc.)
  </li>
</ul>

<p>We also plan to work on frequently asked improvements such as:</p>
<ul>
  <li>Adding playlists to search scope (on PeerTube instance and Sepia Search)</li>
  <li>Subscription to channels/accounts from an instance</li>
  <li>Coordination with, support and contributions to external developments (clients, plugin, etc.)</li>
</ul>

<p>
  More details on our plans can be found on the <a target="_blank" rel="noreferrer noopener"
    href="https://github.com/Chocobozzz/PeerTube/projects/6">PeerTube Github page</a>. Please note that these are the
  main features and developments. They can evolve due to circumstances, contributions, timings, etc. To be informed of
  any change, <a target="_blank" rel="noreferrer noopener"
    href="https://framalistes.org/sympa/subscribe/peertube-newsletter">subscribe to PeerTube newsletter</a>.
</p>

<h4>Managing expectations</h4>

<p>
  This is already a lot, and we can't wait to implement those features!
</p>

<p>
  That being said, we understand that lots of people have lots of expectations for PeerTube. But we won't uphold and
  develop them all, as we are not a tech giant, not even a startup.
</p>

<p>
  <a target="_blank" rel="noreferrer noopener" href="https://framasoft.org/en">Framasoft</a> is a small French not for
  profit of 35 members, and has made the choice not to overgrow. We want to stay at 10 employees tops, even though we
  manage more than 80 different projects (OK, PeerTube is one the biggest ones, but it is still one of eighty projects).
</p>

<p>
  We have only one developer dedicated to PeerTube (not even full time), helped by others in our team (design,
  administration, communication, etc.). Maintaining PeerTube means lots of different tasks (learn more about it <a
    target="_blank" rel="noreferrer noopener" href="https://joinpeertube.org/faq#who-does-what-in-peertube">here in our
    FAQ</a>), and we don't want him (or anyone else, for that matter) to burn himself out to fulfill everyone's
  expectations.
</p>

<p>
  So if our plans for 2021 and PeerTube's v4 don't match your priorities, that's completely OK, but we won't do more nor
  go faster. Remember that you are free to fork PeerTube to lead it in another direction, or to contribute by coding
  plugins and issues (<a target="_blank" rel="noreferrer noopener"
    href="https://docs.joinpeertube.org/contribute/getting-started">get started on contributing here</a>).
</p>

<h4>Thanks for the support</h4>

<p>
  We really want to thank the PeerTube community for its work and support. We also want to thank NLnet for their grant
  that will fund the major part of our work on PeerTube this year, thus relieving us from having to organize a dedicated
  crowdfunding in 2021.
</p>

<p>
  While most of our roadmap for PeerTube is covered by NLnet's donation, the rest will come from our own budget.
  Framasoft is funded by grassroots supporters who give us money for all our actions and projects.
</p>

<p>
  If you want to support PeerTube, you can <a target="_blank" rel="noreferrer noopener"
    href="https://soutenir.framasoft.org/en">support Framasoft with a donation</a>, but also by helping others to
  discover and <a target="_blank" rel="noreferrer noopener" href="https://joinpeertube.org/">learn more about
    PeerTube</a> and our projects: sharing is caring!
</p>
