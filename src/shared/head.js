function buildHead (options) {
  const { title, description, image } = options

  const result = buildHeadTitle(title)

  if (description) {
    result.meta = result.meta.concat(buildHeadDescriptionMeta(description))
  }

  if (image) {
    result.meta = result.meta.concat(buildHeadImageMeta(image))
  }

  return result
}

function buildHeadTitle (titleArg) {
  const title = titleArg + ' | JoinPeerTube'

  return {
    title,

    meta: [
      {
        property: 'og:title',
        content: title
      },
      {
        name: 'twitter:title',
        content: title
      }
    ]
  }
}

function buildHeadDescriptionMeta (description) {
  return [
    {
      name: 'description',
      content: description
    },
    {
      property: 'og:description',
      content: description
    },
    {
      name: 'twitter:description',
      content: description
    }
  ]
}

function buildHeadImageMeta (image) {
  return [
    {
      property: 'og:image',
      content: image
    },
    {
      name: 'twitter:image',
      content: image
    }
  ]
}

function buildHeadArticle (options) {
  const { publishedDate } = options

  const result = buildHead(options)

  result.meta = result.meta.concat([
    {
      name: 'og:type',
      content: 'article'
    },
    {
      name: 'article:published_time',
      content: publishedDate.toISOString()
    },
    {
      name: 'article:modified_time',
      content: publishedDate.toISOString()
    }
  ])

  return result
}

export {
  buildHead,

  buildHeadTitle,
  buildHeadDescriptionMeta,
  buildHeadImageMeta,

  buildHeadArticle
}
