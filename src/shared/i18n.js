function getAllLocales () {
  return Object.keys(getAvailableLocales()).concat(Object.keys(getAliasedLocales()))
}

function getAvailableLocales () {
  return {
    'en_US': 'English',
    'fr_FR': 'Français',
    'ar': 'العربية',
    'de': 'Deutsch',
    'es': 'Español',
    'eo': 'Esperanto',
    'gd': 'Gàidhlig',
    'it': 'Italiano',
    'hr': 'hrvatski',
    'pl': 'Polski',
    'pt_BR': 'Português',
    'ru': 'русский',
    'sq': 'Shqip',
    'sv': 'svenska',
    'hu': 'magyar',
    'gl': 'galego',
    'ja': '日本語',
    'tr': 'Türkçe',
    'th': 'ไทย',
    'zh_Hans': '简体中文（中国）',
    'zh_Hant': '繁體中文（台灣）'
  }
}

function getAliasedLocales () {
  return {
    'en': 'en_US',
    'fr': 'fr_FR',
    'pt': 'pt_BR'
  }
}

function isDefaultLocale (locale) {
  return locale === 'en_US' || locale === 'en'
}

function getDefaultLocale () {
  return 'en_US'
}

function getCompleteLocale (locale) {
  const alias = getAliasedLocales()

  return alias[locale] ?? locale
}

function getShortLocale (locale) {
  return locale.toLowerCase().split('_')[0]
}

export {
  getAllLocales,
  getAvailableLocales,
  isDefaultLocale,
  getAliasedLocales,
  getDefaultLocale,
  getCompleteLocale,
  getShortLocale
}
