import { createRouter, createWebHistory, createMemoryHistory } from 'vue-router'
import Home from './views/Home.vue'
import NotFound from './views/NotFound.vue'
import PublishVideos from './views/PublishVideos.vue'
import Contribute from './views/Contribute.vue'

import newsMeta from './news/meta.json'

import { getAllLocales } from './shared/i18n.js'

async function buildRouter () {
  const SpecificFreshNews = () => import('./views/news/SpecificFreshNews.vue')
  const SpecificArchiveNews = () => import('./views/news/SpecificArchiveNews.vue')
  const News = () => import('./views/news/News.vue')
  const NewsArchive = () => import('./views/news/NewsArchive.vue')
  const AllPluginsSelection = () => import('./views/plugin-selection/AllPluginsSelection.vue')
  const FAQ = () => import('./views/FAQ.vue')
  const BrowseContent = () => import('./views/BrowseContent.vue')
  const Instances = () => import('./views/Instances.vue')

  let routes = []

  for (const locale of [ '' ].concat(getAllLocales())) {
    const base = locale
      ? '/' + locale + '/'
      : '/'

    for (const meta of newsMeta.en) {
      routes.push({
        path: base + 'news/' + meta.id,

        component: meta.isArchive
          ? SpecificArchiveNews
          : SpecificFreshNews,

        meta: {
          id: meta.id
        }
      })
    }

    routes = routes.concat([
      {
        path: '/' + locale,
        component: Home
      },
      {
        path: base + 'help',
        redirect: '/' + locale
      },
      {
        path: base + 'news',
        component: News
      },
      {
        path: base + 'news-archive',
        component: NewsArchive
      },
      {
        path: base + 'instances',
        component: Instances
      },
      {
        path: base + 'faq',
        component: FAQ
      },
      {
        path: base + 'contents-selection',
        redirect: () => {
          return { path: '/' + locale, hash: '#find-peertube-instances' }
        }
      },
      {
        path: base + 'plugins-selection',
        component: AllPluginsSelection
      },
      {
        path: base + 'publish-videos',
        component: PublishVideos
      },
      {
        path: base + 'contribute',
        component: Contribute
      },
      {
        path: base + 'browse-content',
        component: BrowseContent
      },
      {
        path: base + '404',
        component: NotFound
      }
    ])
  }

  routes.push({
    path: '/:pathMatch(.*)*',
    component: NotFound
  })

  return createRouter({
    history: import.meta.env.SSR
      ? createMemoryHistory(import.meta.env.BASE_URL)
      : createWebHistory(import.meta.env.BASE_URL),
    routes,
    scrollBehavior: buildScrollBehaviour()
  })
}

// ---------------------------------------------------------------------------

export {
  buildRouter
}

// ---------------------------------------------------------------------------

function buildScrollBehaviour () {
  if (import.meta.env.SSR) return undefined

  const isBot = /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent)
  const isGoogleCache = window.location.host === 'webcache.googleusercontent.com'

  if (isBot || isGoogleCache) return undefined

  return  (to, _from, savedPosition) => {
    if (savedPosition) return savedPosition
    if (to.hash) return { el: to.hash }

    // Have to use a promise here
    // FIXME: https://github.com/vuejs/router/issues/1411
    return new Promise(res => setTimeout(() => {
      res({ top: 0, left: 0 })
    }))
  }
}
