import { basename } from 'path'
import { renderToString } from 'vue/server-renderer'
import { createApp } from './main'
import { renderHeadToString } from '@vueuse/head'

export async function render(url, manifest = {}, ssrLocale = 'en_US') {
  const { app, router, head } = await createApp(ssrLocale)

  await router.push(url)
  await router.isReady()

  const allRoutes = router.getRoutes().map(r => r.path)

  const ctx = {}
  const html = await renderToString(app, ctx)

  const headTags = await renderHeadToString(head)

  const preloadLinks = renderPreloadLinks(ctx.modules, manifest)
  return { html, preloadLinks, headTags, allRoutes }
}

function renderPreloadLinks(modules, manifest) {
  let links = ''
  const seen = new Set()

  modules.forEach((id) => {
    const files = manifest[id]
    if (!files) return

    files.forEach((file) => {
      if (seen.has(file)) return

      seen.add(file)

      const filename = basename(file)

      if (manifest[filename]) {
        for (const depFile of manifest[filename]) {
          links += renderPreloadLink(depFile)
          seen.add(depFile)
        }
      }

      links += renderPreloadLink(file)
    })
  })

  return links
}

function renderPreloadLink(file) {
  if (file.endsWith('.js') && !file.includes('legacy')) return `<link rel="modulepreload" crossorigin href="${file}">`
  if (file.endsWith('.css')) return `<link rel="stylesheet" href="${file}">`
  if (file.endsWith('.woff')) return ` <link rel="preload" href="${file}" as="font" type="font/woff" crossorigin>`
  if (file.endsWith('.woff2')) return ` <link rel="preload" href="${file}" as="font" type="font/woff2" crossorigin>`
  if (file.endsWith('.gif')) return ` <link rel="preload" href="${file}" as="image" type="image/gif">`
  if (file.endsWith('.jpg') || file.endsWith('.jpeg')) return ` <link rel="preload" href="${file}" as="image" type="image/jpeg">`
  if (file.endsWith('.png')) return ` <link rel="preload" href="${file}" as="image" type="image/png">`

  return ''
}
